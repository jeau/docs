# <img src="/img/minecraft-logo.png" width="25px"> Minecraft App

## About

This app sets up a multiplayer minecraft server.

* Questions? Ask in the [Cloudron Forum - Minecraft Server](https://forum.cloudron.io/category/49/minecraft)
* [Minecraft Server Website](https://minecraft.net/)

# Supported editions

There are 3 different app packages:

* [Minecraft Java Edition Server](https://minecraft.gamepedia.com/Java_Edition)
* [Minecraft Java Edition Forge server](https://forums.minecraftforge.net/)
* [Bedrock/Pocket Edition](https://minecraft.gamepedia.com/Bedrock_Edition)

# Java Edition

## Common commands

Please note that you have to run these commands when the user is logged into the app instance from the Cloudron dashboard.
The username and password are your Cloudron credentials.

* Whitelist a client - `/whitelist minecraft_username`
* Blacklist a client - `/blacklist minecraft_username`
* Become the server operator - `/op your_minecraft_username`
* Reload server after chaning config files like **server.properties** - `/reload`

