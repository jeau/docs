# <img src="/img/ackee-logo.png" width="25px"> Ackee App

## About

Ackee is a Self-hosted, Node.js based analytics tool for those who care about privacy. 

* Questions? Ask in the [Cloudron Forum - CouchPotato](https://forum.cloudron.io/category/118/couchpotato)
* [Ackee Website](https://ackee.electerious.com/)
* [Ackee docs](https://docs.ackee.electerious.com/)
* [Ackee issue tracker](https://github.com/electerious/Ackee/issues)

## Admin Password

The admin password can be changed by editing `/app/data/env` using the [File manager](/apps/#file-manager). Be
sure to restart the app after making the change.

## Adding a domain

* First, add a domain inside Ackee.
* Add [CORS configuration](https://docs.ackee.electerious.com/#/docs/CORS%20headers#platforms-as-a-service-configuration) by
  editing `/app/data/env` and adding your website in `ACKEE_ALLOW_ORIGIN` and restart the app.
* Embed the Ackee tracker.js script in your website.

## Data collection

Ackee won't track personal information (device/browser info) by default, but it has the ability to do so in a privacy focused way.
See Ackee's [docs](https://github.com/electerious/Ackee/blob/master/docs/Anonymization.md#personal-data) for more information.

To enable detailed tracking, pass the `data-ackee-opts` to the script tag:

```
<script async src="https://example.com/ackee-tracker.min.js" 
data-ackee-server="https://example.com" 
data-ackee-domain-id="hd11f820-68a1-11e6-8047-79c0c2d9bce0" 
data-ackee-opts='{ "ignoreLocalhost": true, "detailed": true }'></script>
```

