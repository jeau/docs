# <img src="/img/hedgedoc-logo.png" width="25px"> HedgeDoc App

## About

HedgeDoc is the best platform to write and share markdown.

* Questions? Ask in the [Cloudron Forum - HedgeDoc](https://forum.cloudron.io/category/38/hedgedoc)
* [HedgeDoc Website](https://demo.hedgedoc.org/)
* [HedgeDoc issue tracker](https://github.com/hedgedoc/hedgedoc/issues)

!!! note "Impending rename"
    CodiMD is being renamed to HedgeDoc. This app package will be renamed accordingly.

## Custom configuration

Use the [File manager](/apps#file-manager)
to place custom configuration under `/app/data/config.json`.

See [HedgeDoc docs](https://github.com/hedgedoc/hedgedoc/blob/master/docs/configuration.md)
for configuration options reference.

## Image uploads

By default, images are uploaded to the data directory of the app itself.
To switch to another provider like MinIO, first [configure minio](https://github.com/hedgedoc/hedgedoc/blob/master/docs/guides/minio-image-upload.md) to image uploads. Then, use the following configuration in
`/app/data/config.json`:

```
    "imageUploadType": "minio",
    "s3bucket": "codimd-images",
    "minio": {
        "accessKey": "MINIO_ACCESS_KEY",
        "secretKey": "MINIO_SECRET_KEY",
        "endPoint": "minio.cloudrondomain.com",
        "secure": true,
        "port": 443
    }
```

