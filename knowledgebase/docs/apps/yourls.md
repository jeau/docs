# <img src="/img/yourls-logo.png" width="25px"> YOURLS App

## About

YOURLS is a URL Shortener.

* Questions? Ask in the [Cloudron Forum - Yourls](https://forum.cloudron.io/category/93/yourls)
* [Yourls Website](https://yourls.org)
* [Yourls issue tracker](https://github.com/YOURLS/YOURLS/issues)

## Admin password

If the app is installed without Cloudron user directory integration, the admin password can be changed using the [File Manager](/apps#file-manager) and editing the file `/app/data/user/config.php` and set a new plain text password.

YOURLS will automatically encrypt this field if you refresh your browser as explained [here](https://github.com/YOURLS/YOURLS/wiki/Username-Passwords).

```
$yourls_user_passwords = array(
    'admin' => 'supersecret123' /* Password encrypted by YOURLS */
);
```

## Custom index page

You can edit `/app/data/index.html` using the [File Manager](/apps/#file-manager) to your liking to customize the home page. You can also add a `/app/data/index.php` to
place a PHP script.

## Public Shortner

You can make the URL shortner available to all users. Be aware that a public interface will attract spammers.
YOURLS project comes with a public page that be used for this purpose.

1. Use the [Web terminal](/apps#web-terminal) and set the sample public page as the default page:

```
    cp /app/code/sample-public-front-page.txt /app/data/index.php
```


2. fix the require_once path in `/app/data/index.php` from `dirname(__FILE__).'/includes/load-yourls.php'` to `/app/code/includes/load-yourls.php`.
Like so:

```
    require_once( '/app/code/includes/load-yourls.php' );
```

## Plugins

YOURLS supports a wide variety of [plugins](https://github.com/YOURLS/awesome-yourls#plugins). To install a plugin,
use the [Web terminal](/apps#web-terminal) to unpack a plugin into `/app/data/user/plugins/`. For example,

```
# cd /app/data/user/plugins
# git clone https://github.com/apelly/YourlsBlacklistDomains.git
```

Then, activate the plugin using the `Manage Plugins` link in the admin dashboard.

