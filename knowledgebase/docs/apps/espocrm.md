# <img src="/img/espocrm-logo.png" width="25px"> EspoCRM App

## About

EspoCRM is a web application that allows users to see, enter and evaluate all your company relationships regardless of the type. People, companies, projects or opportunities — all in an easy and intuitive interface.

* Questions? Ask in the [Cloudron Forum - Espo CRM](https://forum.cloudron.io/category/17/espocrm)
* [Espo CRM Website](https://www.espocrm.com/)
* [Espo CRM forum](https://forum.espocrm.com/)
* [Espo CRM issue tracker](https://github.com/espocrm/espocrm/issues)

## Admin access

EspoCRM is automatically setup with an admin account.

```
    username: admin
    password: changeme
```

Be sure to change the admin credentials immediately after installation.

To make an existing Cloudron user an admin, set the admin flag under
`Teams and Access Control`. Currently, this requires that the Cloudron user
log into EspoCRM first.


## Portals

[EspoCRM Portal](https://www.espocrm.com/tips/customer-portals/) is a functionality that provides the access specific CRM data
and functions for your customers and partners. Portals can either be sub paths or standalone domains.

Cloudron supports standalone domains for EspoCRM portals using [App Location Aliases](/apps/#aliases). To setup a portal domain:

* Create the [EspoCRM portal](https://docs.espocrm.com/administration/portal/)
* Specify a custom URL and custom ID for the portal. For example, `https://acme.cloudron.club` in the screenshot below:

    <center>
    <img src="/img/espocrm-portal-config.png" class="shadow">
    </center>

* Add the domain in the Cloudron dashboard.

    <center>
    <img src="/img/espocrm-portal-alias.png" class="shadow">
    </center>

* Add [portal configuration rewrite rules](https://docs.espocrm.com/administration/portal/#access-to-portal) in `/app/data/apache/portals.conf`
  using the [File Manager](/apps#file-manager):

```
    RewriteCond %{HTTP_HOST} ^acme\.cloudron\.club$
    RewriteRule ^client - [L]

    RewriteCond %{HTTP_HOST} ^acme\.cloudron.club$
    RewriteCond %{REQUEST_URI} !^/portal/acme/.*$
    RewriteRule ^(.*)$ /portal/acme/$1 [L]
```

* Restart the app for the apache configuration changes to take effect.

* You can now visit `https://acme.cloudron.club`.

