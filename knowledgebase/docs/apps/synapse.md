# <img src="/img/synapse-logo.png" width="25px"> Synapse App

## About

Matrix is an open network for secure, decentralized communication.

* Questions? Ask in the [Cloudron Forum - Matrix Synapse](https://forum.cloudron.io/category/50/matrix-synapse-riot)
* [Matrix Synapse Website](https://matrix.org)
* [Matrix Synapse issue tracker](https://github.com/matrix-org/synapse/issues)

## Post installation

### Step 1: Select Matrix IDs

Just like email, users on matrix has a unique universal id. These ids are
of the form `@username:domain.com`.

To give users a "memorable id", this app (also known as home server) is pre-setup to use the second level
domain for the domain part of the id (also known as the `server_name`). For example,
if you installed the app at `matrix-homeserver.example.com`, this app package will set the
`server_name` to `example.com`. This will generate user ids of the form `@username:example.com`.

If you require a different server name, use a [File Manager](/apps#file-manager)
to edit `/app/data/configs/homeserver.yaml` and restart the app.

### Step 2: Delegation

Matrix clients and other matrix servers discover the Matrix server for an ID using
[Well-Known](https://www.iana.org/assignments/well-known-uris/well-known-uris.xhtml) URIs.
The Well-Known URI is a document that is served up from the `server_name` domain (i.e `example.com`)
that [delegates](https://github.com/matrix-org/synapse/blob/master/docs/delegate.md) the handling
to another server (i.e `matrix-homeserver.example.com`).

If `server_name` is an app hosted on Cloudron, you can use Cloudron's Well Known URI
support to serve up well-known documents. Go to the `Domains` view and set the Matrix domain in the `Advanced` settings:

<center>
<img src="/img/synapse-wellknown.png" class="shadow">
</center>

Be sure to provide both the hostname and port number (443). To verify the delegation setup, try this command
from your laptop/PC:

```
$ curl https://example.com/.well-known/matrix/server
{ "m.server": "matrix-homeserver.example.com:443" }
```

### Step 3. Federation

[Federation setup](https://github.com/matrix-org/synapse/blob/master/docs/federate.md) is automatic.
Use the [Federation Tester](https://federationtester.matrix.org/) to verify that everything is setup properly.
Note you must enter the `server_name` (like `example.com`) in the form field in the website and NOT the location
of your home server (despite what the form says).

## Admin

To make an existing user an admin, open a [Web terminal](/apps#web-terminal) and
run the following command:

```
PGPASSWORD=${CLOUDRON_POSTGRESQL_PASSWORD} psql -h ${CLOUDRON_POSTGRESQL_HOST} -p ${CLOUDRON_POSTGRESQL_PORT} -U ${CLOUDRON_POSTGRESQL_USERNAME} -d ${CLOUDRON_POSTGRESQL_DATABASE} -c "UPDATE users SET admin=1 WHERE name='@user:example.com'"
```

## Customizations

Synapse offers a variety of [customizations](https://github.com/matrix-org/synapse/blob/develop/docs/sample_config.yaml).
To make changes, use a [File Manager](/apps/#file-manager)
to edit `/app/data/configs/homeserver.yaml` and restart the app.

## Home page

The `index.html` can be customized by editing `/app/data/index.html`. Note that any assets have to be embedded inline.

## Spaces

[MSC1772: Matrix Spaces](https://github.com/matrix-org/matrix-doc/pull/1772) support can be enabled by editing
`/app/data/configs/homeserver.yaml` using the [File Manager](/apps/#file-manager) and adding the following
line:

```
experimental_features: { spaces_enabled: true }
```

Be sure to restart the app after making the change.

