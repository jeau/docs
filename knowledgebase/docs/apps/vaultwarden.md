# <img src="/img/vaultwarden-logo.png" width="25px"> Vaultwarden App

## About

Bitwarden is an Open Source Password Management solution for individuals, teams, and business organizations.
Vaultwarden is an unofficial Bitwarden compatible server written in Rust, fully compatible with the client apps.

* Questions? Ask in the [Cloudron Forum - Vaultwarden](https://forum.cloudron.io/category/64/bitwardenrs)
* [Vaultwarden Website](https://github.com/dani-garcia/vaultwarden)
* [Vaultwarden issue tracker](https://github.com/dani-garcia/vaultwarden/issues)

## Users

Bitwarden does not support Single Sign On. This is by design for security reasons. You must create a new password for your Bitwarden account.

By default, open registration is enabled. This can be changed via environment variables by editing `/app/data/config.env` using
the [File Manager](/apps/#file-manager).

Uncomment the following lines to disable user signup and enable invite only setup:

```
export SIGNUPS_ALLOWED=false
export INVITATIONS_ALLOWED=true
```

## Admin

The admin UI is located `/admin`. To login, use the admin token located at `/app/data/admin_token`.

<center>
<img src="/img/bitwarden-admin.png" class="shadow" width="500px">
</center>

## YubiKey

Vaultwarden has support for [YubiKeys](https://www.yubico.com/). To make use of them, just specify your client id and secret key in the `/app/data/config.env` and restart the app.

```
export YUBICO_CLIENT_ID="clientId"
export YUBICO_SECRET_KEY="secretKey"
```

