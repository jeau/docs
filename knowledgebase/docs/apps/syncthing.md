# <img src="/img/syncthing-logo.png" width="25px"> Syncthing App

## About

Syncthing is a continuous file synchronization program. It synchronizes files between two or more computers in real time, safely protected from prying eyes.

* Questions? Ask in the [Cloudron Forum - Syncthing](https://forum.cloudron.io/category/56/syncthing)
* [Syncthing Website](https://syncthing.net)
* [Syncthing forum](https://forum.syncthing.net/)
* [Syncthing docs](https://docs.syncthing.net/)
* [Syncthing issue tracker](https://github.com/syncthing/syncthing/issues)

## Apps

Complete list of native GUIs and integrations is available [here](https://syncthing.net/).

* [Android app](https://play.google.com/store/apps/details?id=com.nutomic.syncthingandroid)

