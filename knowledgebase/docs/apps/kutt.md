# <img src="/img/kutt-logo.png" width="25px"> Kutt App

## About

Kutt is a free Modern URL Shortener.

* Questions? Ask in the [Cloudron Forum - Kutt](https://forum.cloudron.io/category/111/kutt)
* [Kutt Website](https://kutt.it)

## Custom config

Custom configuration can be put in `/app/data/env` using the [Web Terminal](/apps#web-terminal) or the
[File Manager](/apps/#file-manager). The `env` file contains various customization options with documentation on them.
After changing the file, make sure to restart the app.

## Registration

Registration is enabled by default. This can be disabled by settings `DISALLOW_REGISTRATION=true`
in `/app/data/env`

## Admin

To make a user an admin, edit `/app/data/env` and add the email address to `ADMIN_EMAILS`. For example,

```
ADMIN_EMAILS=test@cloudron.io
```

Note that there is [no separate admin section](https://github.com/thedevs-network/kutt/issues/385#issuecomment-704322774).
When a user is admin, they can view all the links in the table in the front page. Look for the 'All links' checkbox.

<center>
<img src="/img/kutt-admin.png" class="shadow">
</center>

## 3rd party packages

Kutt integrates with a variety of languages and frameworks. See [upstream docs](https://github.com/thedevs-network/kutt#3rd-party-packages)
for more information

## Custom domains

Kutt supports having more than one domain. You can add domains in the custom domain section. Note
`Set domain` below is a bit misleading because it's really `Add domain`.

<center>
<img src="/img/kutt-custom-domain.png" class="shadow">
</center>

Then, in the Cloudron Dashboard add the custom domains as domain aliases in the `Location` view:

<center>
<img src="/img/kutt-domain-aliases.png" class="shadow">
</center>


