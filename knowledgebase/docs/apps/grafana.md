# <img src="/img/grafana-logo.png" width="25px"> Grafana App

## About

Grafana is the open and composable observability and data visualization platform.

* Questions? Ask in the [Cloudron Forum - Grafana](https://forum.cloudron.io/category/98/grafana)
* [Grafana Website](https://grafana.com)
* [Grafana forum](https://community.grafana.com/)
* [Grafana issue tracker](https://github.com/grafana/grafana/issues)

## Customizations

Custom configuration can be added in the file `/app/data/custom.ini` using the [File Manager](/apps/#file-manager).
See the [Grafana docs](https://grafana.com/docs/grafana/latest/administration/configuration/)
on the various configuration options. Be sure to restart the app after making any changes.

## Installing plugins

To install plugins, you run the grafana CLI tool using the [Web Terminal](/apps#web-terminal).

For example,

```
# /app/code/bin/grafana-cli -homepath /app/code -config /run/grafana/custom.ini plugins install grafana-worldmap-panel
```

## Reset admin password

```
# /app/code/bin/grafana-cli --homepath /app/code  --config /run/grafana/custom.ini admin reset-admin-password secret123 
```

