# <img src="/img/openvpn-logo.png" width="25px"> OpenVPN App

## About

OpenVPN provides flexible VPN solutions to secure your data communications, whether it's for
Internet privacy, remote access for employees, securing IoT, or for networking Cloud data centers

* Questions? Ask in the [Cloudron Forum - OpenVPN](https://forum.cloudron.io/category/20/openvpn)
* [OpenVPN Website](https://openvpn.org/)
* [Upstream OpenVPN forum](https://community.openvpn.net/openvpn)

## Desktop and Mobile Clients

The OpenVPN app has been tested with the following clients:

* NetworkManager on Ubuntu
* [Tunnelblick](https://www.tunnelblick.net/) on Mac OS X
* [OpenVPN for Android](https://play.google.com/store/apps/details?id=de.blinkt.openvpn)

## How to connect on Ubuntu 16.04

* Install the Network Manager OpenVPN plugin
```
sudo apt-get install network-manager-openvpn-gnome
```

* Download the .ovpn embedded certs config file from the OpenVPN app

<center>
<img src="/img/openvpn-config.png" class="shadow">
</center>

* Open Network Manager, `VPN Settings` -> `Import from file...`

## Admin Settings

The admin panel can be used to customize some of the popular settings like
the network address and client-to-client connectivity.

To make a user an admin, edit the file `/app/data/config.ini` and add
the username to the `admins` key. Note that you have to restart the app
and re-login for the admin role to take effect.

<center>
<img src="/img/openvpn-settings.png" class="shadow">
</center>

## DNS Server

This app has a built-in Dnsmasq DNS server (which is pushed to clients). This DNS server
allows resolution of connected clients using `devicename.username`.

You can configure this DNS server by editing `/app/data/dnsmasq.conf` using the (/apps#file-manager).
For example, to make Dnsmasq forward DNS requests to an internal DNS server, use the following:

```
server=internal-server-ip
```

See [Dnsmasq docs](http://thekelleys.org.uk/dnsmasq/docs/dnsmasq-man.html) for all the available options.

## Customizations

You can customize various settings by editing `/app/data/openvpn.conf` using
the  [File Manager](/apps#file-manager). Some popular options are
discussed below:

### Custom routes

By default, clients are configured to route all traffic via the VPN. If you disable this, you would
want to push custom routes for the network and hosts behind the VPN. For example, edit the file as below and
restart the app.

```
# push "redirect-gateway def1 bypass-dhcp"
push "route 178.128.183.220 255.255.255.255"
push "route 178.128.74.0 255.255.255.0"
```

## Privacy

The OpenVPN app provides a tunnel to channel all the traffic from your
devices via the Cloudron. Websites and services that you visit will
not see the IP address of your devices but they *will* see the IP
address and possibly the RDNS hostname of your Cloudron.

You can check what sort of information can be gathered from your
Cloudron's IP address using [ipleak.net](https://ipleak.net).

## Custom Client Configuration

Custom Client Configuration allows the OpenVPN admin to assign a specific IP address to a client or push specific options 
such as compression and DNS server to a client.

To add custom settings:

* Edit `/app/data/openvpn.conf` and add the following line:
```
client-config-dir /app/data/ccd
```

* Create the directory `/app/data/ccd`

* You can create custom client configs in this directory by creating files with the name `[username]_[devicename]`. You can also
  create a file named `DEFAULT` which will be used if no device specific file exists.

* For example, to assign a static IP to a client, you can add the line `ifconfig-push 10.8.0.50 10.8.0.51` (requires IP pair)

* Restart the app for changes to take effect.

## Troubleshooting

If you are unable to connect to the OpenVPN server, make sure that your VPS firewall
allows the OpenVPN port (by default, this is 7494/TCP). For example, you might have
to add this incoming port as part of EC2 security group.

