# Mailbox Sharing

## Overview

Cloudron 6 introduced IMAP mailbox sharing. This feature will work by default
on new Cloudron 6 installations. For Cloudrons that were installed before 6.0,
some manual steps are required to make mailbox sharing work.

## Background

Cloudron uses dovecot as the IMAP server. The default namespace separator in
dovecot is `.`. However, this separator conflicts with the mailbox sharing feature
This is because Cloudron Email server users the user's email as the username and email
addresses have a `.` in the domain portion.

The solution is to migrate to using `/` as the namespace separator. New installations use this separator
by default. Existing installations must be migrated manually to use this separator.

!!! warning "Automated migration"
    The reason this migration is not automated is because there is a small chance that
    email clients might have cached this separator. For such clients, one will have to
    re-add the email account. At the time of writing, we are collecting information on
    what possible mail clients will be affected. Roundcube, Rainloop, SOGo, Thunderbird
    do not have a problem, but you might have to logout and login again. On Android K-9,
    one has to 'Refresh Folder List' on the account.

## Pre-flight

Before proceeding, please check what separator is used by the mail server. You can
do this by inspecting the file `/home/yellowtent/boxdata/mail/dovecot/namespace_separator`.
If it has `/`, then you can skip this guide entirely. If it has `.`, then read on.

## Migrate

SSH into the server and run the following commands to migrate to `/` separator.

```
cd /home/yellowtent/boxdata/mail/vmail
find . -type f -name '*.sieve' -exec sed -e '/fileinto/{s,\.,/,g}' -i.bak '{}' \;
echo "/" > /home/yellowtent/boxdata/mail/dovecot/namespace_separator
docker restart mail
```

The above script creates a backup of all the sieve scripts. You can safely delete them
once all your users have reported back that there are no issues.

```
find . -type f -name '*.sieve.bak' -exec rm '{}' \;
```

